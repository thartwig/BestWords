from readT import ReadText

def TrySavedTweets(Nw0):
    print("Let's see if there are enough tweets already stored in the file...")

    Nw = 0
    div = 0
    i = 0

    with open("TT.txt", "r") as TTfile:
        TT = []#Trump's tweets

        for line in TTfile:
            TT.append(line)
            i += 1

            if(len(TT)>10):#otherwise, sorting does not work
                Nw,div = ReadText(str(TT))
            if(Nw >= Nw0):
                break

    return Nw, div, i
