import tweepy
from tweepy import OAuthHandler
from readT import ReadText
from Get_Tweets import GetTweets

def TryTwitter(Nw0):
    print("We need to download additional tweets...")
    
    #load access key/tokens from file. You need to set your own information here.
    with open("KeyToken.txt") as f:
        keys = f.readlines()
    #remove whitespace characters like `\n` at the end of each line
    keys = [x.strip() for x in keys]

    consumer_key = keys[1]#'YOUR-CONSUMER-KEY'
    consumer_secret = keys[2]#'YOUR-CONSUMER-SECRET'
    access_token = keys[3]#'YOUR-ACCESS-TOKEN'
    access_secret = keys[4]#'YOUR-ACCESS-SECRET'
     
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)
     
    api = tweepy.API(auth)

#    number_of_tweets=100
    TT = []#Trump's tweets

    file = open("TT.txt",'w')#write tweets into file
    Nw = 0
    IDmax = -1
    i = 0
    while(Nw < Nw0):
        tweets = GetTweets(api,IDmax)
        for t in tweets:#go through currently loaded tweets
            i += 1
            IDmax = t.id
            TT.append(t.full_text)
            file.write(t.full_text+"\n")
            
            if(len(TT)>10):#otherwise, sorting does not work
                Nw,div = ReadText(str(TT))
            if(Nw >= Nw0):
                break
    file.close()

    return Nw, div, i
