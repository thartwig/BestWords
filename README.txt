### "I'm very highly educated. I know words, I have the best words." ###
###                            Donald Trump                          ###
###                                                                  ###
###                       REALLY? Let's find out...                  ###
###                                                                  ###
###                         Tilman Hartwig (2020)                    ###

During the 2016 presidential campaign, then-candidate Donald Trump said at a campaign stop in South Carolina on Dec. 30, 2015, that he has the best words:
https://www.washingtonpost.com/video/national/trump-i-have-the-best-words/2017/04/05/53a9ae4a-19fd-11e7-8598-9a99da559f9e_video.html

Let's use natural language processing and a bit of data science to find out if this is true. This small program compares the complexity of any text (measured by the diversity of words) to the complexity of Donald Trump's tweets

While it may appear as a slightly political message to show that the most powerful man in the world is highly overestimating its own competence, this program is solely intended as inspiration for data science.


Trump claims to "have the best words", but a simple text analysis shows that a 16-year-old climate activist from Sweden has a larger English lexis than Trump. Also featuring Hannah Arendt, Simone de Beauvoir, and others. Spoiler: everyone has "better words" than Trump!

Also interesting:
"TRUMP SPEAKS AT FOURTH-GRADE LEVEL, LOWEST OF LAST 15 U.S. PRESIDENTS, NEW ANALYSIS FINDS: https://www.newsweek.com/trump-fire-and-fury-smart-genius-obama-774169"


### USAGE ###
To run the program, open a terminal, change the directory ("cd") to the folder with this and the other files, and then type:
python main.py
KeyToken.txt : If you want to use the most recent Tweets, set your key and token in this file.
main.py : You can set the author that you want to compare to in this file. You can add your own authors here. The program will first check if enough Tweets are already saved in the TT.txt file. Otherwise, it will download additional Trump tweets.
readT.py : most of the natural language processing happens here. Print messages can be (de)activated here.

### REQUIREMENTS ###
You need python and the following libraries:
numpy
nltk
tweepy
You do not need a twitter account to use this program. Sufficient Trump tweets (to compare to all off-the-shelf implemented texts and speeches) are saved in the file TT.txt. However, if you want to download the most recent tweets or compare to longer texts, you need to set your twitter account info in the file KeyToken.txt.

### LICENSE ###
Love data science, hate capitalism!
You can do with this program whatever you like, but please don't use any part of it for commercial purposes.
See also LICENSE.


### DISCLAIMER ###
This little program may have bugs and various caveats: are tweets comparable to speeches, books, or historical documents? Are my way of natural language processing and definition of "word diversity" reasonable? Did I intentionally select authors and texts that have a higher word complexity than Trump's tweets?

### ACKNOWLEDGEMENTS ###
Thanks to Josie & Vanessa for testing the code.