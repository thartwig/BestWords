#Take a list of characters, "clean" the words, and calculate the word diversity.
import nltk, re, pprint
import numpy as np
from nltk import word_tokenize
from nltk.corpus import stopwords

def ReadText(CharList):
    tokens = word_tokenize(CharList)
#    print(tokens[:10])
    if 1:#remove capitalisation: AMAZING = amazing
        tokens = [token.lower() for token in tokens]
    if 1:#remove stopwords
        stop = set(stopwords.words('english'))
        words = tokens
        tokens = [word for word in words if word not in stop]
    if 1:#remove websites and "&"
        words = tokens
        tokens = [word for word in words if (word != 'https' and word != 'amp')]
    if 1:#remove all words that are not alphabetic
        tokens = [word for word in tokens if word.isalpha()]

#Possible Improvement: Stemming with PorterStemmer?

    words = tokens
    
    NwordsA = len(words)
#    print("All words:",NwordsA)
    UniqueWords,counts = np.unique(words,return_counts = True)
    NwordsU = len(counts)
#    print("Unique words:",NwordsU)
#    print("Diversity:",100*NwordsU/NwordsA,"%")

#Print Top 10 Words.
#First, sort list accordingly
    if 1:
        SortedCounts, SortedWords = (list(x) for x in zip(*sorted(zip(counts,UniqueWords))))
        SortedCounts.reverse()
        SortedWords.reverse() 
        print("Top 10 words:")
        for i in range(np.min([len(SortedCounts),10])):
            print(SortedCounts[i]," times ",SortedWords[i])

#The word diversity is simply defined as the number of unique words divided by the number of all words.
#This could clearly be improved or played around with.

    return NwordsA,100*NwordsU/NwordsA
