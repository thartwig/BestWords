#main part of the program

#load other functions
from readT import ReadText
from Try_Twitter import TryTwitter
from Try_Saved_Tweets import TrySavedTweets

#If nltk libraries are missing, try this:
#import nltk
#nltk.download('punkt')
#from nltk import word_tokenize,sent_tokenize
#nltk.download('stopwords')

#Set the author
author = "Greta"

#Set file that contains text to compare
if(author == "Greta"):
    file = "Greta.txt"#Greta Thunberg "Our House is on Fire" 2019 World Economic Forum (WEF) in Davos
elif(author == "Arendt"):
    file = "Arendt.txt"#Hanna Arendt, Introduction to "War and Revolution"
elif(author == "de Beauvoir"):
    file = "deBeauvoir.txt"#Simone de Beauvoir, Introduction to "The Second Sex"
elif(author == "Obama"):
    file = "Obama.txt"#Obama's Nobel Peace Price Speech
elif(author == "Declaration of Independence"):
    file = "DeclarationOfIndependence.txt"#US, 1776
else:
    print("Sorry, author "+author+" not known.")

#Read this file
with open(file, 'r') as f:
    CharList = f.read()
Nw0,div0 = ReadText(CharList)
#Nw0: number of words from author
#div0: word diversity from author

#To be useful for users without a twitter account, I provide a ist of the latest Tweets
#Check if sufficient tweets are already saved.
Nw, div, i = TrySavedTweets(Nw0)
#Nw: number of words from Trump
#div: word diversity from Trump
#i: tweets collected
if(Nw >= Nw0):
    print("Yes, we already have enough tweets.")
else:#not enough tweets, download new tweets
    Nw, div, i = TryTwitter(Nw0)

#Compare
print(author+"'s speach has "+str(Nw0)+" words and a diversity of "+'{:5.5}'.format(str(div0))+"%.")
print("To obtain the same number of words, we have to collect Trump's last "+str(i)+ " tweets.")
print("These tweets have "+str(Nw)+" words and a diversity of "+'{:5.5}'.format(str(div))+"%.")

#Conclude
if(div0 > div):
    winner = author
else:
    winner = "Trump"

print(winner+" has better words.")
