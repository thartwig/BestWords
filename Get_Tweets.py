import numpy as np
import tweepy

def GetTweets(api,MaxID):
    number_of_tweets=3500
    username = 'realDonaldTrump'

#the number of tweets that can be loaded at once is limited. Therefore, we need to load them in batches and remember the MaxID from the last loaded tweet.
    if(MaxID < 0):
        tweets = api.user_timeline(screen_name=username, count = number_of_tweets, include_rts = False, tweet_mode = 'extended') 
    else:
        tweets = api.user_timeline(screen_name=username, include_rts = False, tweet_mode = 'extended', count = number_of_tweets,max_id=MaxID-1) 

    return tweets
